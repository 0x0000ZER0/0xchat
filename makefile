main: server client

server:
	gcc -g server.c handler.c -o 0xSERVER -Wall -pedantic

client:
	gcc -O2 client.c -o 0xCLIENT -Wall -pedantic -lpthread

clear:
	rm 0xCLIENT 0xSERVER
