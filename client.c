#include <stdio.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "decl.h"

static void
run(uint16_t port, char *name) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1) {
		perror("ERROR: Couldn't create the SOCKET");
		return;
	}

	sockaddr_in server;
	memset(&server, 0, sizeof (sockaddr_in));

	server.sin_family = AF_INET;
	server.sin_port   = htons(port);
	server.sin_addr.s_addr = htonl(0x7f000001); // 127.0.0.1

	packet   data;
	strcpy(data.name, name);

	char    *ret;
	size_t   msg_len;
	ssize_t  sent_len; 
	int 	 res;

	sendto(fd, (void*)&data, sizeof (data), 0, (sockaddr*)&server, sizeof (server));
		
	pid_t child;
	child = fork();

	if (child == 0) {
		while (true) {
			recvfrom(fd, (void*)&data, sizeof (data), 0, NULL, NULL);		

			printf("\r[%s] said: %s\n", data.name, data.buff);
		}
	} else {
		do {
			ret = fgets(data.buff, BUFF_LEN, stdin);
			if (ret == NULL) {
				perror("ERROR: couldn't read the message.");
				continue;
			}

			msg_len = strlen(data.buff);
			if (msg_len <= 1)
				continue;
	
			data.buff[msg_len - 1] = '\0';

			sent_len = sendto(fd, (void*)&data, sizeof (data), 0, (sockaddr*)&server, sizeof (server));
			if (sent_len == -1)
				perror("ERROR: Couldn't send the data");

			res = strcmp(data.buff, "/q"); 
		} while (res != 0);

		res = close(fd);
		if (res == -1) {
			perror("ERROR: Couldn't close the SOCKET");
			return;
		}
	}
}

int
main(int argc, char **argv) {
	if (argc < 2) {
		perror("ERROR: specifing the PORT and the NAME is mandatory.");
		return -1;
	}

	int port;
	port = strtol(argv[1], NULL, 10);
	if (port == LONG_MIN || port == LONG_MAX) {
		perror("ERROR: couln't parse the given PORT");
		return -1;
	}

	run(port, argv[2]);

	return 0;
}
