#include "decl.h"

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "stdio.h"

void
handler_init(handler *hnd) {
	hnd->cap  = 4;
	hnd->len  = 0;
	hnd->list = malloc(sizeof (client) * 4); 

	int i;
	i = 0;
	while (i < hnd->cap) {
		hnd->list[i].is_active = false;
		++i;
	}
}

client*
handler_new(handler *hnd) {
	int i;
	i = 0;
	while (i < hnd->cap) {
		if (hnd->list[i].is_active == false) {
			++hnd->len;			
			return &hnd->list[i];
		}
	
		++i;
	}

	return NULL;
}

client*
handler_get(handler *hnd, char *name) {
	int i;
	i = 0;

	while (i < hnd->cap) {
		if (hnd->list[i].is_active == true) {
			int cmp;
			cmp = strcmp(hnd->list[i].name, name);

			if (cmp == 0)
				return &hnd->list[i];
		}
		++i;
	}

	return NULL;
}

void
handler_free(handler *hnd) {
	free(hnd->list);
}
