# 0xCHAT

An `UDP` based CLI chat.

### How to run

- build the `SERVER` and `CLIENT` programs using `make` command.

`make`

- run the newly created programs.

`0xSERVER MY_PORT`

`0xCLIENT MY_PORT MY_NAME`

### Example

- running the server:

```c
./0xSERVER 1919
```

- running the client:

```c
./0xCLIENT 1919 zer0
```

- Finally start typing.
