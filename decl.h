#ifndef DECL_H
#define DECL_H

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdbool.h>

#define BUFF_LEN 512
#define NAME_LEN 64

typedef struct sockaddr    sockaddr;
typedef struct sockaddr_in sockaddr_in;

typedef struct {
	char name[NAME_LEN];
	char buff[BUFF_LEN];
} packet;

typedef struct {
	sockaddr_in  sock;
	char 	    *name;	
	bool 	     is_active;
} client;

typedef struct {
	int 	len;
	int 	cap;
	client *list;
} handler;

void
handler_init(handler*);

client*
handler_new(handler*);

client*
handler_get(handler*, char*);

void
handler_free(handler*);

#endif
