#include <stdio.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "decl.h"

static void 
run(uint16_t port) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1) {
		perror("ERROR: couldn't create the SOCKET.");
		return;
	}	

	sockaddr_in server;	
	memset(&server, 0, sizeof (server));
	
	server.sin_family 	= AF_INET;
	server.sin_port   	= htons(port);
	server.sin_addr.s_addr  = htonl(INADDR_ANY);

	int err;
	err = bind(fd, (sockaddr*)&server, sizeof (server));
	if (err == -1) {
		perror("ERROR: couldn't bind to the given ADDRESS.");
		return;
	}

	packet data;
	ssize_t	len;

	sockaddr_in client_sock;
	socklen_t   client_len;
	client_len = sizeof (sockaddr_in);

	handler hnd;
	handler_init(&hnd);
	do {		
		len = recvfrom(fd, (void*)&data, sizeof (data), 0, (sockaddr*)&client_sock, &client_len);

		client *cln;
		cln = handler_get(&hnd, data.name);

		if (cln == NULL) {
			printf("INFO: new client[%s] connected.\n", data.name);
			cln = handler_new(&hnd);

			cln->sock = client_sock;
			cln->name = strdup(data.name);
			cln->is_active = true;
			continue;
		}	

		if (len == -1) {
			perror("ERROR: couldn't receive the DATA.");
		} else {
			printf("INFO: client [%s] sent: %s\n", data.name, data.buff);

			for (int i = 0; i < hnd.cap; ++i) {
				if (hnd.list[i].is_active == true) {
					int cmp;
					cmp = strcmp(cln->name, hnd.list[i].name);

					if (cmp != 0) {
						//printf("INFO: sent message to [%s]\n", hnd.list[i].name);
						//printf("      from client [%s]\n", data.name);
						//printf("      saying: %s\n", data.buff);

						sendto(fd, (void*)&data, sizeof (data), 0, (sockaddr*)&hnd.list[i].sock, sizeof (sockaddr_in));
					}
				}
			}
		}
	} while (true);
	handler_free(&hnd);

	err = close(fd);
	if (err == -1) {
		perror("ERROR: couldn't close the SOCKET.");
		return;
	}
}

int
main(int argc, char **argv) {
	if (argc < 2) {
		perror("ERROR: you must specify the PORT number.");
		return -1;
	}
	
	uint16_t port;

	port = strtol(argv[1], NULL, 10);
	if (port == LONG_MIN || port == LONG_MAX) {
		perror("ERROR: couldn't convert the PORT");
		return -1;
	}

	run(port);

	return 0;
}
